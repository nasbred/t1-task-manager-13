package ru.t1.kharitonova.tm.api.controller;

public interface IProjectTaskController {
    void bindTaskToProject();

    void unbindTaskToProject();
}
